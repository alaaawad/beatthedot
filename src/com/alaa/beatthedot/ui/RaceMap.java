package com.alaa.beatthedot.ui;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.alaa.beatthedot.R;
import com.alaa.beatthedot.database.PathPlot;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class RaceMap {
	private GoogleMap gMap;
	private PathPlot pathPlot;
	private SplitPolyline splitPolyline = new SplitPolyline();
	private boolean locked = false;
	private int primaryColor = Color.RED;
	private int secondaryColor = Color.BLACK;
	private Marker startMarker;
	private Marker endMarker;
	private Marker splitMarker;
	
		
	public RaceMap(GoogleMap gMap, PathPlot pathPlot) {
		this.gMap = gMap;
		this.pathPlot = pathPlot;
	}
	

	
	public void addEndPoint(LatLng point) {
		addMarker(point, "End");
		addSegment(point);
	}
	
	public void addPoint(LatLng point) {
		//add a plotted path point - used when someone clicks or long clicks map
		if (size() == 0) {
			addMarker(point, "Start");
			this.pathPlot.add(point);
		} else {
			gMap.addMarker(new MarkerOptions().position(point)
			    .icon(BitmapDescriptorFactory.fromResource(R.drawable.dot)).anchor((float)0.5, (float)0.5));
			addSegment(point);
		}
	}
	
	public Marker addMarker(LatLng point, String text) {
		return gMap.addMarker(new MarkerOptions().position(point).title(text));
	}
	
	private void addSegment(LatLng point) {
		this.pathPlot.add(point);
		drawNoSplitAsPrimary();
	}
	
	public void resetLineColors() {
		
	}
	
	private LatLng interpolatePoint(LatLng begin, LatLng end, double ratio) {
		// find middle latlng point
		double midLat = begin.latitude + (end.latitude - begin.latitude) * ratio;
		double midLong = begin.longitude + (end.longitude - begin.longitude) * ratio;
		return new LatLng(midLat, midLong);
	}
	
	public void draw(ArrayList<LatLng> primaryLinePoints, ArrayList<LatLng> secondaryLinePoints) {
		// create lines
		Polyline primaryLine = null;
		Polyline secondaryLine = null;
		if (primaryLinePoints.size() > 0) {
			primaryLine = gMap.addPolyline(
					(new PolylineOptions().color(primaryColor)).addAll(primaryLinePoints) );
		}
		if (secondaryLinePoints.size() > 0) {
			secondaryLine = gMap.addPolyline(
					(new PolylineOptions().color(secondaryColor)).addAll(secondaryLinePoints) );
		}
		
		// remove old lines
		this.splitPolyline.clear();
		
		// update splitPolyline
		this.splitPolyline.setPrimaryLine(primaryLine);
		this.splitPolyline.setSecondaryLine(secondaryLine);
	}
	
	public void draw(double distance) {
		if (locked) {
			Log.w("PathPlot", "Attempting to call draw() before last call has finished.");
			return;
		}
		locked = true;
		try {
			drawSplitAt(distance);
		} finally {
			locked = false;
		}
	}
	
	public void drawNoSplitAsPrimary() {
		draw(this.pathPlot.getPoints(), new ArrayList<LatLng>());
	}

	public void drawNoSplitAsSecondary() {
		draw(new ArrayList<LatLng>(), this.pathPlot.getPoints());
	}
	
	private void drawSplitAt(double distance) {
		ArrayList<LatLng> primaryLinePoints = new ArrayList<LatLng>();
		ArrayList<LatLng> secondaryLinePoints = new ArrayList<LatLng>();
		
		// find the segment to split at
		double remainingDistance = distance;
		double ratioOfSegment = 0;
		int segment = -1;
		
		// add start point
		primaryLinePoints.add(this.pathPlot.get(0));
		// place holder for split point
		secondaryLinePoints.add(null);
		
		// iterate over segment end points to construct lines
		for (int i = 1; i < this.pathPlot.size(); i++) {
			if (segment != -1 || distance == 0) {
				// already completed split
				secondaryLinePoints.add(this.pathPlot.get(i));
				continue;
			}
			// consume next segment
			double segmentDistance = this.pathPlot.getSegmentDistance(i-1);
			double distanceIntoSegment = remainingDistance;
			remainingDistance -= segmentDistance;
			if (remainingDistance < 0) {
				// found it
				segment = i-1;
				ratioOfSegment = distanceIntoSegment / segmentDistance;
				secondaryLinePoints.add(this.pathPlot.get(i));
			} else {
				// still searching
				primaryLinePoints.add(this.pathPlot.get(i));
			}
		}

		if (segment != -1) {
			// we have a split segment. find the split point and add it to the lines
			LatLng splitPoint = interpolatePoint(this.pathPlot.get(segment), this.pathPlot.get(segment+1), ratioOfSegment);
			primaryLinePoints.add(splitPoint);
			secondaryLinePoints.set(0, splitPoint);  // overwrite placeholder
			
			Marker oldMarker = splitMarker;

			splitMarker = addMarker(splitPoint, "a");
			if(oldMarker != null) {
				oldMarker.remove();
			}
			
			draw(primaryLinePoints, secondaryLinePoints);
		} else {
			// no split
			if (distance == 0) {
				// imaginary split is at first point
				drawNoSplitAsSecondary();
			} else {
				// imaginary split is at last point
				drawNoSplitAsPrimary();
			}
		}
	}
	
	
	public void clear(){
		this.pathPlot.clear();
		splitPolyline.clear();
	}
	
	public int size() {
		return this.pathPlot.size();
	}

	public GoogleMap getgMap() {
		return gMap;
	}

	public void setgMap(GoogleMap gMap) {
		this.gMap = gMap;
	}

	public int getPrimaryColor() {
		return primaryColor;
	}

	public void setPrimaryColor(int primaryColor) {
		this.primaryColor = primaryColor;
	}

	public int getSecondaryColor() {
		return secondaryColor;
	}

	public void setSecondaryColor(int secondaryColor) {
		this.secondaryColor = secondaryColor;
	}



	public PathPlot getPathPlot() {
		return pathPlot;
	}

	public void setPathPlot(PathPlot pathPlot) {
		this.pathPlot = pathPlot;
	}
	
}
