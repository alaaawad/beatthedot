package com.alaa.beatthedot.database;

import java.util.Calendar;
import java.util.List;

import android.graphics.Color;

import com.alaa.beatthedot.util.Pace;

public class Racer {

	Racer (){
		
	}
	
	private Pace overallPace;
	private Pace currentSplitPace;
	private List<Pace> splitPaces;
	private Calendar splitTime;  // set to be the user�s current gps polling rate
	private Color color;
	private Calendar finishTime;
	
	public Pace getOverallPace() {
		return overallPace;
	}
	public void setOverallPace(Pace overallPace) {
		this.overallPace = overallPace;
	}
	public Pace getCurrentSplitPace() {
		return currentSplitPace;
	}
	public void setCurrentSplitPace(Pace currentSplitPace) {
		this.currentSplitPace = currentSplitPace;
	}
	public List<Pace> getSplitPaces() {
		return splitPaces;
	}
	public void setSplitPaces(List<Pace> splitPaces) {
		this.splitPaces = splitPaces;
	}
	public Calendar getSplitTime() {
		return splitTime;
	}
	public void setSplitTime(Calendar splitTime) {
		this.splitTime = splitTime;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public Calendar getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Calendar finishTime) {
		this.finishTime = finishTime;
	}

	
}
