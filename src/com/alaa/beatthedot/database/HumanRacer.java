package com.alaa.beatthedot.database;

import com.alaa.beatthedot.util.Pace;

public class HumanRacer extends Racer{
	
	private RacerTrace trace;
	private Pace constantPace;
	
	public HumanRacer(RacerTrace trace){
		this.setTrace(trace);
	}

	public RacerTrace getTrace() {
		return trace;
	}

	public void setTrace(RacerTrace trace) {
		this.trace = trace;
	}

	public Pace getConstantPace() {
		return constantPace;
	}

	public void setConstantPace(Pace constantPace) {
		this.constantPace = constantPace;
	}
	
	
	
	
	
}