package com.alaa.beatthedot.database;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

public class PathPlot implements Parcelable {
	private static final int EarthRadius = 6371; //km
	
	private ArrayList<LatLng> points = new ArrayList<LatLng>();
	private ArrayList<Double> segmentDistances = new ArrayList<Double>();
	private double totalDistance = 0;

    public static final Parcelable.Creator<PathPlot> CREATOR = new Parcelable.Creator<PathPlot>() {
        public PathPlot createFromParcel(Parcel in) {
            return new PathPlot(in);
        }

        public PathPlot[] newArray(int size) {
            return new PathPlot[size];
        }
    };

		
    public PathPlot() {
    	
    }
    
    public PathPlot(Parcel in) {
    	in.readTypedList(points, LatLng.CREATOR);
    	calculateDistances();  // populates segmentDistances and totalDistance
    }
    
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(points);
	}
	

	public double calculateDistance(LatLng pointA, LatLng pointB){
		/*
		 * using Haversine formula to calculate distance 'as the crow flies' 
		between two points across spherical globe known as earth
		 *
		 */
		double dLat = (pointB.latitude - pointA.latitude) * Math.PI/180;
		double dLon = (pointB.longitude-pointA.longitude) * Math.PI/180;
		double lat1 = pointA.latitude * Math.PI/180;
		double lat2 = pointB.latitude * Math.PI/180;

		double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = EarthRadius * c;
		return distance * 0.621371;
	}
	
	public double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.##");
	    return Double.valueOf(twoDForm.format(d));
	}
		
	private void calculateDistances() {
		totalDistance = 0;
		segmentDistances.clear();
		LatLng iterPoint;
		LatLng lastIterPoint;
		if (size() > 1){
			Iterator<LatLng> iterPnt = this.points.iterator();
			lastIterPoint = iterPnt.next();
			while (iterPnt.hasNext()){
				iterPoint = iterPnt.next();
				double dist = calculateDistance(lastIterPoint, iterPoint);
				segmentDistances.add(dist);
				totalDistance += dist;
				lastIterPoint = iterPoint;
			}
		}
	}
	
	public double getTotalDistance(){
		return totalDistance;
	}
	
	
	public int size() {
		return points.size();
	}

	public  ArrayList<LatLng>  getPoints() {
		return points;
	}

	public void setPoints(ArrayList<LatLng>  mPoints) {
		this.points = mPoints;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void add(LatLng point) {
		points.add(point);
		LatLng prior = get(size()-1);
		double dist = calculateDistance(prior, point);
		segmentDistances.add(dist);
		totalDistance += dist;
	}

	public LatLng get(int i) {
		return points.get(i);
	}
	
	public double getSegmentDistance(int i) {
		return this.segmentDistances.get(i);
	}

	public void clear() {
		this.points.clear();
		this.segmentDistances.clear();
		this.totalDistance = 0;
		
	}
}
