package com.alaa.beatthedot.database;

import com.alaa.beatthedot.util.Pace;

public class ComputerRacer extends Racer{
	
	private Pace constantPace;
	
	public ComputerRacer(Pace constantPace){
		this.setConstantPace(constantPace);
	}
	public Pace getConstantPace() {
		return constantPace;
	}

	public void setConstantPace(Pace constantPace) {
		this.constantPace = constantPace;
	}
	
	
}