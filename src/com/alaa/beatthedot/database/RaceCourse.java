package com.alaa.beatthedot.database;


import android.os.Parcel;
import android.os.Parcelable;


public class RaceCourse implements Parcelable {
	
	
	private PathPlot pathPlot;

	public RaceCourse(PathPlot pathPlot) { 
		this.pathPlot = pathPlot;
	}

	public RaceCourse(Parcel in) {
		this.pathPlot = in.readParcelable(PathPlot.class.getClassLoader());
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// dont know why this doesnt work. corrupts the parcel somehow.
		dest.writeParcelable(this.pathPlot, 0);
	}
	
	public PathPlot getPathPlot() {
		return this.pathPlot;
	}

	public void setPathPlot(PathPlot pathPlot) {
		this.pathPlot = pathPlot;
	}
  
 
	@Override
	public int describeContents() {
		return 0;
	}
 

    public static final Parcelable.Creator CREATOR =
    	new Parcelable.Creator() {
            public PathPlot createFromParcel(Parcel in) {
                return new PathPlot(in);
            }
 
            public PathPlot[] newArray(int size) {
                return new PathPlot[size];
            }
        };

}
	