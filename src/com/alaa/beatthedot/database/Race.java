package com.alaa.beatthedot.database;

import java.util.Calendar;
import java.util.List;

public class Race{
	private List<Racer> racers;
	private PathPlot pathPlot;
	private Calendar startTime;
	private Calendar endTime;

	public Race(PathPlot pathPlot) {
		this.pathPlot = pathPlot;
	}
	
	public List<Racer> getRacers() {
		return racers;
	}

	public void setRacers(List<Racer> racers) {
		this.racers = racers;
	}

	public PathPlot getPathPlot() {
		return pathPlot;
	}

	public void setPathPlot(PathPlot pathPlot) {
		this.pathPlot = pathPlot;
	}

	public Calendar getStartTime() {
		return startTime;
	}

	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	public Calendar getEndTime() {
		return endTime;
	}

	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}
	
}