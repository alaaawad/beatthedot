package com.alaa.beatthedot.activity;

import java.text.DateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.alaa.beatthedot.R;

public class SetRacePaceActivity extends Activity {
    private Button timeBtn;
	private Button paceBtn;
	DateFormat formatDateTime=DateFormat.getDateTimeInstance();
	Calendar dateTime=Calendar.getInstance();
	private TextView timeLabel;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pace);
        timeLabel=(TextView)findViewById(R.id.timeTxt);
        updateLabel();
    }
    public void choosePace(View view){
    	new TimePickerDialog(this, d, dateTime.get(Calendar.HOUR), dateTime.get(Calendar.MINUTE), true).show();
    }
    
    public void chooseTime(View view){
    	new TimePickerDialog(this, t, dateTime.get(Calendar.HOUR), dateTime.get(Calendar.MINUTE), true).show();
    }
    
    TimePickerDialog.OnTimeSetListener d=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int minutes, int seconds) {
			dateTime.set(Calendar.MINUTE, minutes);
			dateTime.set(Calendar.SECOND,seconds);
			updateLabel();
		}
	};
	
	TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hours, int minutes) {
			dateTime.set(Calendar.HOUR, hours);
			dateTime.set(Calendar.MINUTE,minutes);
			updateLabel();
		}
	};
	private void updateLabel() {
		timeLabel.setText(formatDateTime.format(dateTime.getTime()));
	}
}