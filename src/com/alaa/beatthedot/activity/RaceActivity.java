package com.alaa.beatthedot.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alaa.beatthedot.R;
import com.alaa.beatthedot.database.PathPlot;
import com.alaa.beatthedot.ui.RaceMap;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;

public class RaceActivity extends FragmentActivity implements 
	OnCameraChangeListener, OnInitListener{

	private GoogleMap gMap;
	// remove later. this should come from raceCourse
    private PathPlot pathPlot = null;
	private RaceMap rMap;
	private static final int pace = 10; 
	private TextView timer_text;
	private double progress = 0;
	TextToSpeech talker;
    long starttime = 0;
    
    
    
    private Handler handler = new Handler() {
    	  public void handleMessage(Message msg) {
	        	progress += 10;
	        	rMap.draw(progress);
    	  }
    };
    Runnable runRace = new Runnable() {
        @Override
        public void run() {
        	handler.obtainMessage().sendToTarget();
        	handler.postDelayed(this, 100);
        }
    };

    
   // timer thread
   Handler h2 = new Handler();
   Runnable run = new Runnable() {

        @Override
        public void run() {
           long millis = System.currentTimeMillis() - starttime;
           int seconds = (int) (millis / 1000);
           int minutes = seconds / 60;
           seconds     = seconds % 60;
           
           // update timer text
           timer_text.setText(String.format("%d:%d:%d", minutes, seconds, millis));
           
           //draw dot
           if (seconds==20){
        	   say("you have 1 min left");
           }

           h2.postDelayed(this, 50);
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_race);
        pathPlot = getIntent().getExtras().getParcelable("PathPlotParcel");
        setUpMapIfNeeded();
        rMap = new RaceMap(gMap,pathPlot);
        try {
			this.rMap.drawNoSplitAsPrimary();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        timer_text = (TextView)findViewById(R.id.text);
        talker = new TextToSpeech(this, this);
        Button b = (Button)findViewById(R.id.btnRace);
        b.setText("start");
        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Button b = (Button)v;
                if(b.getText().equals("stop")){
                    h2.removeCallbacks(run);
                    handler.removeCallbacks(runRace);
                    b.setText("start");
                }else{
                    starttime = System.currentTimeMillis();
                    h2.postDelayed(run, 0);
                    handler.post(runRace);
                    b.setText("stop");
                }
            }
        }); 
        
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();    
    }
    
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (gMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            gMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (gMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        //mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        gMap.setOnCameraChangeListener(this);
        gMap.setMyLocationEnabled(true);
    }

    public void say(String text2say){
    	talker.speak(text2say, TextToSpeech.QUEUE_FLUSH, null);
    }

	@Override
	public void onInit(int status) {
		
	}
	
	@Override
	public void onDestroy() {
		if (talker != null) {
			talker.stop();
			talker.shutdown();
		}

		super.onDestroy();
	}



	@Override
	public void onCameraChange(CameraPosition arg0) {
		// TODO Auto-generated method stub
		
	}
    
}