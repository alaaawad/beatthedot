package com.alaa.beatthedot.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alaa.beatthedot.R;
import com.alaa.beatthedot.database.PathPlot;
import com.alaa.beatthedot.ui.RaceMap;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class DrawMapPathActivity extends FragmentActivity 
	implements OnMapClickListener, OnMapLongClickListener, OnCameraChangeListener{ 
    
	private GoogleMap gMap;
    private PathPlot pathPlot = new PathPlot();
	private RaceMap rMap;
    private Button raceButton;
	private TextView mTapTextView;
    private TextView mCameraTextView;
    private boolean racing = false;
    private int markerCnt=0;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_map_path);
        setUpMapIfNeeded();
        rMap = new RaceMap(gMap,pathPlot);
        
        mTapTextView = (TextView) findViewById(R.id.tap_text);
        mCameraTextView = (TextView) findViewById(R.id.camera_text);
        raceButton = (Button) findViewById(R.id.race_button);   
    }

    public void onRaceClick(View view) {
		Intent intent = new Intent(this, RaceActivity.class);
		intent.putExtra("PathPlotParcel", rMap.getPathPlot());
		startActivity(intent);
    	/*
    	if (!racing) {
			myTimer.schedule(new TimerTask() {			
				@Override
				public void run() {
					if (markerCnt > 1) {
						handler.obtainMessage().sendToTarget();
					}
				}
			}, 0, 100);
			racing = true;
			raceButton.setText(R.string.button_racing);
    	} else {
    		myTimer.cancel();
    		raceButton.setText(R.string.button_race);
    	}*/
    }
    

    
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        
    }
    

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #gMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView
     * MapView}) will show a prompt for the user to install/update the Google Play services APK on
     * their device.
     * <p>
     * A user can return to this Activity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the Activity may not have been
     * completely destroyed during this process (it is likely that it would only be stopped or
     * paused), {@link #onCreate(Bundle)} may not be called again so we should call this method in
     * {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (gMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            gMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (gMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p>
     * This should only be called once and when we are sure that {@link #gMap} is not null.
     */
    private void setUpMap() {
        //mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        gMap.setOnMapClickListener(this);
        gMap.setOnMapLongClickListener(this);
        gMap.setOnCameraChangeListener(this);
        gMap.setMyLocationEnabled(true);
    }


    @Override
    public void onMapClick(LatLng point) {
        if(markerCnt == 1){
        	rMap.addPoint(point);
	    }
        displayMessage();
        
    }

    @Override
    public void onMapLongClick(LatLng point) {
    	if (markerCnt == 0){
    		rMap.addPoint(point);
    		markerCnt++;
    	}else if(markerCnt == 1){
    		rMap.addEndPoint(point);
    		markerCnt++;
    	}else{
    		gMap.clear();
    		rMap.clear();
    		markerCnt=0;
    	}
    	displayMessage();
    	
    }

    @Override
    public void onCameraChange(final CameraPosition position) {
        //mCameraTextView.setText(position.toString());
    }


    public void displayMessage(){
    	if (rMap.getPathPlot().size() == 0){
        	mTapTextView.setText("Long press the map to add a start point.");
    		mCameraTextView.setText("Total Distance: " + Double.toString( rMap.getPathPlot().getTotalDistance())+ " miles");
    	}else{
        	mTapTextView.setText("Now tap the map to draw a path");
        	mCameraTextView.setText("Total Distance: " + Double.toString( rMap.getPathPlot().getTotalDistance()) + " miles");
    	}
    }

    
}
