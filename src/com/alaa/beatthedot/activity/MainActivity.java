package com.alaa.beatthedot.activity;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.alaa.beatthedot.R;
import com.alaa.beatthedot.view.MenuItemView;

/**
 * The main activity of the API library menuitem gallery.
 * <p>
 * The main layout lists the menuitem features, with buttons to launch them.
 */
public final class MainActivity extends ListActivity {

    /**
     * A simple POJO that holds the menuitems about the menuitem that are used by the List Adapter.
     */
    private static class MenuItem {
        /**
         * The resource id of the title of the menuitem.
         */
        private final int titleId;

        /**
         * The resources id of the description of the menuitem.
         */
        private final int descriptionId;

        /**
         * The menuitem activity's class.
         */
        private final Class<? extends android.support.v4.app.FragmentActivity> activityClass;

        public MenuItem(int titleId, int descriptionId,
                Class<? extends android.support.v4.app.FragmentActivity> activityClass) {
            super();
            this.titleId = titleId;
            this.descriptionId = descriptionId;
            this.activityClass = activityClass;
        }
    }

    /**
     * A custom array adapter that shows a {@link MenuItemView} containing menuitems about the menuitem.
     */
    private static class CustomArrayAdapter extends ArrayAdapter<MenuItem> {

        /**
         * @param menuitems An array containing the menuitems of the menuitems to be displayed.
         */
        public CustomArrayAdapter(Context context, MenuItem[] menuitems) {
            super(context, R.layout.feature, R.id.title, menuitems);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MenuItemView featureView;
            if (convertView instanceof MenuItemView) {
                featureView = (MenuItemView) convertView;
            } else {
                featureView = new MenuItemView(getContext());
            }

            MenuItem menuitem = getItem(position);

            featureView.setTitleId(menuitem.titleId);
            featureView.setDescriptionId(menuitem.descriptionId);

            return featureView;
        }
    }

    private static final MenuItem[] menuitems = {new MenuItem(
            R.string.draw_path, R.string.basic_description_path, DrawMapPathActivity.class),
            new MenuItem(R.string.view_history, R.string.basic_description_history,
            		ViewMapPathHistoryActivity.class)};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ListAdapter adapter = new CustomArrayAdapter(this, menuitems);

        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        MenuItem menuitem = (MenuItem) getListAdapter().getItem(position);
        startActivity(new Intent(this, menuitem.activityClass));
    }
}
