package com.alaa.beatthedot.util;


public class Pace {
	private int pace; // sec/mile
	private int speed = 1/pace;
	public Pace (int pace){
		this.pace = pace;
	}
	
	public int getPace(){
		return pace;
	}
	
	public void setPace(int pace){
		this.pace = pace;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
}
