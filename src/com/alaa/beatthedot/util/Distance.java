package com.alaa.beatthedot.util;

import java.util.Calendar;

public class Distance {
	private double dist;
	public Distance (double dist){
		this.dist = dist;
	}
	
	public double getDistance(){
		return dist;
	}
	
	public void setDistance(double dist){
		this.dist = dist;
	}

	public double getSpeed(Calendar time) {
		return dist/time.get(Calendar.SECOND);
	}
	
}
